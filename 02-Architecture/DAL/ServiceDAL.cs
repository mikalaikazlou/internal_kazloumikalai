﻿using System.Collections.Generic;

namespace DAL
{
    public class ServiceDAL
    {
        public List<CountryEntity> GetCountryEntity()
        {
            var result = new List<CountryEntity>();
            result.Add(new CountryEntity { Name = "Belarus" });
            result.Add(new CountryEntity { Name = "Germany" });
            result.Add(new CountryEntity { Name = "The_USA" });
            result.Add(new CountryEntity { Name = "Brasil" });
            result.Add(new CountryEntity { Name = "Poland" });
            return result;
        }
    }
}