﻿using DAL;
using System.Collections.Generic;

namespace BL
{
    public class ServiceBL
    {
        public List<Country> GetListCountry()
        {
            var entityDal = new ServiceDAL().GetCountryEntity();
            var countries = new List<Country>();

            foreach (var item in entityDal)
            {
                string nameCountry = item.Name + "BL";
                countries.Add(new Country { Name = nameCountry });
            }
            return countries;
        }
    }
}