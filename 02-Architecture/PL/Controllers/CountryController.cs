﻿using BL;
using Microsoft.AspNetCore.Mvc;
using PL.Models;

namespace PL.Controllers
{
    public class CountryController : Controller
    {
        
        public IActionResult Index()
        {
            var _countriesBL = new ServiceBL().GetListCountry();
            var _countryModels = new ServicePL().CountryModels(_countriesBL);
            string result = "";
            foreach (var item in _countryModels)
            {
                result += item.Name;
            }
            return Content(result);
        }
    }
}