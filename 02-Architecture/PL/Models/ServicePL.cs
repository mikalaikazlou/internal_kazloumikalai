﻿using BL;
using System.Collections.Generic;

namespace PL.Models
{
    public class ServicePL
    {
        public List<CountryModel> CountryModels(List<Country> ct)
        {
            var _countryModels = new List<CountryModel>();

            foreach (var item in ct)
            {
                string nameCountry = item.Name + "PL; ";
                _countryModels.Add(new CountryModel { Name = nameCountry });
            }
            return _countryModels;
        }
    }
}